$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/Home.feature");
formatter.feature({
  "name": "User should be able to see COVID-19 resources",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User should be able click on COVID-19 resources",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user on home page",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "The user click on the red highlighted link",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "The url should be \"https://www.antiochian.org/dashboard?name\u003dCOVID-19\"",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});