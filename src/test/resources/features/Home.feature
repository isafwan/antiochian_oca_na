Feature: User should be able to see COVID-19 resources


  Scenario: User should be able click on COVID-19 resources
    Given The user on home page
    When The user click on the red highlighted link
    Then The url should be "https://www.antiochian.org/dashboard?name=COVID-19"
